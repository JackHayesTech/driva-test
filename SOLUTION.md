# Driva code test
## UI
Due to the time constraints imposed I would have like to have done many thing better/more. These include
* A few components/functions are overly complicated and I would like to decouple them/split them up.
* Better error messages when entering invalid data that gives more details on what is wrong with the application.
* Better way to check if user has finished typing than the on blur method since when the user is finished they have to click out in order to submit/progress to the next screen.
* Any css styling at all.
* Tests
* Some sort of message detailing that the request has been sent successfully.
## Backend
Due to the time constraints imposed I would have like to have done many thing better/more. These include
* Swagger validation for requests.
* A better request structure for the data.
* More edge cases, I can think of several instances where a fatal error may occur.
* 100% test coverage.