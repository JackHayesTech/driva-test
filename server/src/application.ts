import express, { json } from 'express';
import cors from 'cors';
import { config } from 'dotenv';

import { routes } from './api/routes';
import { dbConnect } from './utilities/db-connect';

// Loads the env vars
config();

// Creates the express application.
const app = express();

// Creates the db connection
const connection = dbConnect();

// Enables json request data.
app.use(json());

// Enable cors
app.use(cors());
app.options('*', cors());

// Applies the api routes to the application.
routes(app, connection);

export default app;
