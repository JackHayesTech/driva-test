import { Connection } from 'mysql';
import { v4 as uuidv4 } from 'uuid';
import { getValue } from '../utilities/get-value';
import { InputData } from '../types';

/**
 * Saves the application to the database.
 * @param db - Database connection
 * @param body - The request body
 */
export const saveApplication = async (db: Connection, body: InputData[]) => {
  // Returns data needed for the db query from the request body.
  const mob = getValue(body, 'mobile');
  const email = getValue(body, 'email');

  const date = new Date().toISOString().slice(0, 19).replace('T', ' ');
  const uuid = uuidv4();

  // Template string for the request
  const sql = `INSERT INTO quotes
  (uuid, status, mobile, email, quote_data, created_at)
  VALUES(?, 0, ?, ?, ?, ?)`;

  // Executes the db request.
  await db.query(sql, [uuid, mob, email, JSON.stringify(body), date]);
};
