import { Request, Response } from 'express';
import { Connection } from 'mysql';

import { saveApplication } from './save-application.controller';

/**
 * Sets the route up for saving the application.
 * @param req - Request
 * @param res - Express response object
 * @param db - DB connection
 */
export const saveApplicationRoute = async (
  req: Request,
  res: Response,
  db: Connection
) => {
  const { body } = req;
  try {
    await saveApplication(db, body);
  } catch (error) {
    // Error occurred when saving application.
    console.log(error);
    res.status(500).json({ error: error.toString() });
    return;
  }
  res.json({ message: 'Application saved successfully.' });
};
