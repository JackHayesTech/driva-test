import express, { Application } from 'express';
import request from 'supertest';

jest.mock('../save-application.route');

import { routes } from '../routes';
import { saveApplicationRoute } from '../save-application.route';
import { applicationRoute } from '../../data';

describe('Tests for the routes file.', () => {
  // Setup the application routes for testing.
  const app: Application = express();
  routes(app, null as any);

  it('Should register the post route.', async () => {
    const mockFunc = (req: Request, res: any, db: any): void => {
      res.status(200).json();
    };
    (saveApplicationRoute as jest.Mock).mockImplementation(mockFunc);
    const res = await (request(app) as any)['post'](applicationRoute);
    expect(res.status).toEqual(200);
  });
});
