jest.mock('../save-application.controller');

import { saveApplicationRoute } from '../save-application.route';
import { saveApplication } from '../save-application.controller';

describe('Tests for the save application route file.', () => {
  const req: any = { body: 'test' };
  it('Should call the mock with the expected data when successful.', async () => {
    const mock = jest.fn();
    const res = { json: mock };
    await saveApplicationRoute(req, res as any, null as any);
    expect(mock).toHaveBeenCalledWith({
      message: 'Application saved successfully.',
    });
  });

  it('Should call the mock with the expected data when error.', async () => {
    (saveApplication as jest.Mock).mockImplementation(() => {
      throw new Error('');
    });
    const mock = jest.fn();
    const res = {
      status: jest.fn().mockImplementation(() => ({
        json: mock,
      })),
    };
    await saveApplicationRoute(req, res as any, null as any);
    expect(mock).toHaveBeenCalledWith({
      error: 'Error',
    });
  });
});
