import { Application, Request, Response } from 'express';
import { Connection } from 'mysql';

import { saveApplicationRoute } from './save-application.route';
import { applicationRoute } from '../data';

/**
 * Assigns the application routes.
 * @param app - The express application.
 */
export const routes = (app: Application, db: Connection): void => {
  app
    .route(applicationRoute)
    // Creates a application.
    .post((req: Request, res: Response) => saveApplicationRoute(req, res, db));
};
