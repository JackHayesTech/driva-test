import { InputData } from '../types';

/**
 * Return a value from a input data array.
 * @param data - The data to parse.
 * @param key - The key to search for.
 * @returns The value of the key pair.
 */
export const getValue = (data: InputData[], key: string): string => {
  const obj = data.filter((d) => d.id === key);
  if (!obj[0])
    throw new Error(
      `Data with required id: ${key} was not found in request body.`
    );
  return obj[0].value;
};
