import { createConnection, Connection } from 'mysql';

/**
 * Connects to the database
 * @returns DB connection
 */
export const dbConnect = (): Connection => {
  const {
    env: { DB_USER, DB_PASSWORD, DB_DATABASE, DB_HOST, DB_PORT },
  } = process;

  const connection = createConnection({
    host: DB_HOST,
    port: parseInt(DB_PORT),
    user: DB_USER,
    password: DB_PASSWORD,
    database: DB_DATABASE,
  });

  connection.connect();

  return connection;
};
