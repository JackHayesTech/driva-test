import { dbConnect } from '../db-connect';

jest.mock('mysql');
import { createConnection } from 'mysql';

describe('Tests for the db connect utility.', () => {
  it('Should call the mock with the expected data.', () => {
    const mock = jest.fn(() => ({ connect: jest.fn() }));
    (createConnection as jest.Mock).mockImplementation(mock);

    const user = 'user';
    const password = 'pass';
    const database = 'db';
    const host = 'host';
    const port = '1111';

    process.env.DB_USER = user;
    process.env.DB_PASSWORD = password;
    process.env.DB_DATABASE = database;
    process.env.DB_HOST = host;
    process.env.DB_PORT = port;

    dbConnect();

    expect(mock).toHaveBeenCalledWith({
      host,
      port: parseInt(port),
      user,
      password,
      database,
    });
  });
});
