import { getValue } from '../get-value';

describe('Tests for the get value utility function.', () => {
  const value = 'val';
  const id = 'key';

  const data: any[] = [{ id, value }];

  it('Should return the expected data.', () => {
    const res = getValue(data, id);
    expect(res).toEqual(value);
  });

  it('Should throw error when key is not found.', () => {
    expect(() => getValue(data, 'test')).toThrow();
  });
});
