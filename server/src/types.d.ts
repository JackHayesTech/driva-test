// Represents a the structure for a input object.
export interface InputData {
  id: string;
  value: string;
}
