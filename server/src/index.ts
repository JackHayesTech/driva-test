import http from 'http';
import application from './application';

const port = 8080;

// Creates the server
http
  .createServer(application)
  .listen(port, () => console.log(`Http app is running on port ${port}`));
