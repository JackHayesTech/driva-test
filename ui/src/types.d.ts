export interface BaseInput {
  id: string;
  label: string;
  required: boolean;
  orderNum: number;
  value?: string;
}

export interface SelectValue {
  name: string;
  label: string;
}

export interface SelectInput extends BaseInput {
  type: 'select';
  values: Array<SelectValue>;
}

export interface StringInput extends BaseInput {
  type: 'text';
  placeholder: string;
  validation?: string;
}

export type InputType = StringInput | SelectInput;

export interface PageData {
  title: string;
  inputs: Array<InputType>;
  pageNumber: number;
}
