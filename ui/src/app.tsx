import React, { useState } from 'react';

import { Page } from 'components/page';
import { Nav } from 'components/nav';
import { pages } from './page-data';
import { postData, checkValid, getPage, setPageData } from 'utilities';

const App: React.FC = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [pagesData, setPages] = useState(pages);
  const [pageValid, setValid] = useState(false);

  /**
   * Moves backwards or forwards in the pages.
   * @param num
   */
  const updatePage = (num: 1 | -1) => {
    setCurrentPage(currentPage + num);
    const page = getPage(pagesData, currentPage + num);
    checkValid(page, pageValid, setValid);
  };

  /**
   * Updates the page data with then new input data.
   * @param pagesData
   */
  const updateInput = (pageNum: number, input: number, value: string) => {
    const data = setPageData(pagesData, pageNum, input, value);
    setPages(data);
    const page = getPage(pagesData, currentPage);
    checkValid(page, pageValid, setValid);
  };

  return (
    <div>
      <Page page={getPage(pagesData, currentPage)} updateInput={updateInput} />
      <Nav
        currentPage={currentPage}
        numPages={pagesData.length}
        updatePage={updatePage}
        pageValid={pageValid}
        submitData={() => postData(pagesData)}
      />
    </div>
  );
};

export default App;
