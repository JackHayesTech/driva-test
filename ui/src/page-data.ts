import { PageData } from 'types';

// Doesn't allow numbers.
const nameValidation = '^[^0-9]*$';
const phoneValidation = '^[0-9]*$';

const emailValidation = `^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$`;

const page1: PageData = {
  title: 'Tell us about yourself',
  pageNumber: 1,
  inputs: [
    {
      id: 'firstName',
      orderNum: 1,
      type: 'text',
      required: true,
      label: 'First name',
      placeholder: 'As it appears on your licence',
      validation: nameValidation,
    },
    {
      id: 'middleName',
      orderNum: 2,
      type: 'text',
      required: false,
      label: 'Middle name',
      placeholder: 'Optional',
      validation: nameValidation,
    },
    {
      id: 'lastName',
      orderNum: 3,
      type: 'text',
      required: true,
      label: 'Last name',
      placeholder: 'As it appears on your licence',
      validation: nameValidation,
    },
    {
      id: 'mobile',
      orderNum: 4,
      type: 'text',
      required: true,
      label: 'Mobile number',
      placeholder: '+61',
      validation: phoneValidation,
    },
    {
      id: 'email',
      orderNum: 5,
      type: 'text',
      required: true,
      label: 'Email',
      placeholder: 'Please enter a valid email',
      validation: emailValidation,
    },
  ],
};

const page2: PageData = {
  title: 'Tell us about yourself',
  pageNumber: 2,
  inputs: [
    {
      id: 'relationshipStatus',
      orderNum: 1,
      type: 'select',
      required: true,
      label: 'Relationship status',
      values: [
        { name: 'single', label: 'Single' },
        { name: 'defacto', label: 'Defacto' },
        { name: 'married', label: 'Married' },
      ],
      value: 'single',
    },
    {
      id: 'income',
      orderNum: 2,
      type: 'text',
      required: true,
      label: 'Income after tax',
      placeholder: 'Income after tax',
    },
    {
      id: 'incomeFrequency',
      orderNum: 3,
      type: 'select',
      required: true,
      label: 'Income frequency',
      values: [
        { name: 'annually', label: 'Annually' },
        { name: 'monthly', label: 'Monthly' },
        { name: 'weekly', label: 'Weekly' },
        { name: 'daily', label: 'Daily' },
      ],
      value: 'annually',
    },
    {
      id: 'occupation',
      orderNum: 4,
      type: 'text',
      required: true,
      label: 'Occupation',
      placeholder: 'Occupation',
    },
    {
      id: 'currentEmployer',
      orderNum: 5,
      type: 'text',
      required: true,
      label: 'Current Employer',
      placeholder: 'Current Employer',
    },
    {
      id: 'empYears',
      orderNum: 6,
      type: 'text',
      required: true,
      label: 'Years at current employer',
      placeholder: 'Years at current employer',
    },
    {
      id: 'empMonths',
      orderNum: 7,
      type: 'text',
      required: true,
      label: 'Months at current employer',
      placeholder: '1-12',
    },
    {
      id: 'Dependents',
      orderNum: 8,
      type: 'text',
      required: true,
      label: 'Dependents',
      placeholder: '1-10',
    },
  ],
};

export const pages: Array<PageData> = [page1, page2];
