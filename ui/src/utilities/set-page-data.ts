import { PageData } from '../types';

/**
 * Updates the page data with the new value for the specified object
 * @param pagesData
 * @param pageNum
 * @param input
 * @param value
 * @returns
 */
export const setPageData = (
  pagesData: PageData[],
  pageNum: number,
  input: number,
  value: string
): PageData[] => {
  pagesData.forEach((pd) => {
    if (pd.pageNumber === pageNum)
      pd.inputs.forEach((i) => {
        if (i.orderNum === input) i.value = value;
      });
  });
  return pagesData;
};
