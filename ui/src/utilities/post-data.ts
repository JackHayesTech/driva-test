import { PageData } from 'types';

/**
 * Sends the page data to the api
 * @param data
 */
export const postData = async (data: PageData[]) => {
  // Gets the input data and flattens it into an array.
  const body = data
    .map((d) =>
      d.inputs
        .map(({ id, value }) => ({
          id,
          value,
        }))
        .filter((i) => i.value !== undefined)
    )
    .flat();

  // Constructs request.
  const requestOptions = {
    method: 'POST',
    headers: { 'content-type': 'application/json', Accept: 'application/json' },
    body: JSON.stringify(body),
  } as any;

  // Sends request.
  await fetch('http://localhost:8080/application', requestOptions);
};
