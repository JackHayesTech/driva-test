import { PageData } from 'types';

/**
 * Parses page data to see if valid or not.
 * @param page - The page data to be checked
 * @param pageValid - The current value of if the page is valid or not.
 * @param setValid - Function to change the page valid value.
 */
export const checkValid = (
  page: PageData,
  pageValid: Boolean,
  setValid: Function
) => {
  // Parses each value to see if it is set and if it is required.
  const valid = !page.inputs.some((i) => i.required && !i.value);
  // Checks if the value has changed in order to avoid re render.
  if (valid !== pageValid) setValid(valid);
};
