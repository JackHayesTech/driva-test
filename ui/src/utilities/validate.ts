/**
 * Checks if the input is valid or not.
 * @param input - The input string.
 * @param optional - If the field is optional or not.
 * @param validator - Regex string to check input against.
 * @returns true or false
 */
export const validate = (
  input: string,
  isRequired: boolean,
  validator: string = '.'
): boolean => {
  // The field is optional and no input has been entered so no check is needed.
  if (!isRequired && input === '') return true;
  return !!input.match(validator);
};
