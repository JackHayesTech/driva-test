export { validate } from './validate';
export { postData } from './post-data';
export { checkValid } from './check-valid';
export { setPageData } from './set-page-data';
export { getPage } from './get-page';
