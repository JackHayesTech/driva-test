import { PageData } from 'types';

/**
 * Returns the page with the specified pageNumber
 * @param data
 * @param pageNumber
 * @returns
 */
export const getPage = (pages: PageData[], pageNumber: number) =>
  pages.filter((p) => p.pageNumber === pageNumber)[0];
