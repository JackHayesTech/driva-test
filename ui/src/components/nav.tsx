import React from 'react';

interface Nav {
  currentPage: number;
  numPages: number;
  updatePage: Function;
  pageValid: boolean;
  submitData: Function;
}

interface Button {
  text: string;
  fun: Function;
  disabled?: boolean;
}

const Button = ({ text, fun, disabled = false }: Button) => (
  <input
    type="button"
    value={text}
    onMouseUp={() => fun()}
    disabled={disabled}
  />
);

export const Nav = ({
  currentPage,
  numPages,
  updatePage,
  pageValid,
  submitData,
}: Nav) => (
  <div>
    {currentPage !== 1 ? (
      <span>
        <Button text="Back" fun={() => updatePage(-1)} />
      </span>
    ) : null}
    <span>
      {currentPage !== numPages ? (
        <Button text="Next" disabled={!pageValid} fun={() => updatePage(1)} />
      ) : (
        <Button text="Submit" disabled={!pageValid} fun={() => submitData()} />
      )}
    </span>
  </div>
);
