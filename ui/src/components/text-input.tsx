import React from 'react';

interface TextInput {
  id: string;
  placeholder: string;
  value: string;
  validateInput: Function;
  updateInput: Function;
}

export const TextInput = ({
  id,
  placeholder,
  value,
  validateInput,
  updateInput,
}: TextInput) => (
  <input
    id={id}
    name={id}
    placeholder={placeholder}
    onChange={(e) => validateInput(e.target.value)}
    onBlur={(e) => updateInput(e.target.value)}
    value={value}
  />
);
