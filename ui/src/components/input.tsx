import React, { useState } from 'react';

import { InputType } from 'types';
import { TextInput } from './text-input';
import { SelectInput } from './select-input';
import { validate } from 'utilities';

interface Input {
  // The data for the input.
  data: InputType;
  // Function to set the new input in the base data.
  updateInput: Function;
}

/**
 * Base input component.
 */
export const Input = ({
  data: { value, id, label, required },
  data,
  updateInput,
}: Input) => {
  const [val, setValue] = useState(value ? value : '');
  const [valid, setValid] = useState(true);

  // Validates the data against the validation regex string.
  const validateInput = (
    input: string,
    validation: string | undefined = undefined
  ) => {
    setValue(input);
    if (validate(input, required, validation)) setValid(true);
    else setValid(false);
  };

  const updateData = () => (valid ? updateInput(val) : null);

  return (
    <>
      <label htmlFor={id}>{label}</label>
      <br />
      {(() => {
        switch (data.type) {
          case 'text':
            return (
              <TextInput
                id={id}
                value={val}
                validateInput={(e: string) => validateInput(e, data.validation)}
                updateInput={updateData}
                placeholder={data.placeholder}
              />
            );
          case 'select':
            return (
              <SelectInput
                id={id}
                value={val}
                values={data.values}
                validateInput={(e: string) => validateInput(e)}
                updateInput={updateData}
              />
            );
          default:
            throw new Error('Invalid input type has been used.');
        }
      })()}
      {valid ? null : <span>*Error invalid input.</span>}
      <br />
    </>
  );
};
