import React from 'react';

import { PageData, InputType } from 'types';
import { Input } from './input';

interface Page {
  page: PageData;
  updateInput: Function;
}

const sortComponents = (a: InputType, b: InputType) => a.orderNum - b.orderNum;

export const Page = ({
  page: { inputs, title, pageNumber },
  updateInput,
}: Page) => (
  <>
    <h2>{title}</h2>
    {inputs.sort(sortComponents).map((input) => (
      <Input
        data={input}
        key={input.id}
        updateInput={(value: string) =>
          updateInput(pageNumber, input.orderNum, value)
        }
      />
    ))}
  </>
);
