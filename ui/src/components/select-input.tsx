import React from 'react';
import { SelectValue } from 'types';

interface SelectInput {
  id: string;
  value: string;
  validateInput: Function;
  updateInput: Function;
  values: Array<SelectValue>;
}

export const SelectInput = ({
  id,
  value,
  validateInput,
  updateInput,
  values,
}: SelectInput) => (
  <select
    id={id}
    name={id}
    onChange={(e) => validateInput(e.target.value)}
    onBlur={(e) => updateInput(e.target.value)}
    value={value}
  >
    {values.map((val) => (
      <option value={val.name} key={val.name}>
        {val.label}
      </option>
    ))}
  </select>
);
